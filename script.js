customElements.define('slider-item',
    class extends HTMLElement {
        constructor() {
            super();

            const template = document.getElementById('product-template');
            const templateContent = template.content;
            let source = this.querySelector('.item__photo').src;
            templateContent.querySelector('.slide__photo').src = source;

            const shadowRoot = this.attachShadow({mode: 'open'});

            shadowRoot.appendChild(templateContent.cloneNode(true));
        }
    }
);

customElements.define('product-slider',
    class Slider extends HTMLElement {
        constructor() {
            super();
            this.slideIndex = 1;

            const template = document.getElementById('product-slider-template');
            const templateContent = template.content;

            const shadowRoot = this.attachShadow({mode: 'open'});


            shadowRoot.appendChild(templateContent.cloneNode(true));

            this.browsing(this.slideIndex);
            this.shadowRoot.querySelector('.button_prev').onclick = (evt) => {
                evt.preventDefault();
                this.plusSlides(-1);
            };
            this.shadowRoot.querySelector('.button_next').onclick = (evt) => {
                evt.preventDefault();
                this.plusSlides(-1);
            };
        }

        browsing(n) {
            let slides = document.querySelectorAll('.slider__item');

            if (n > slides.length) {
                this.slideIndex = 1
            }
            if (n < 1) {
                this.slideIndex = slides.length
            }
            for (let i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slides[this.slideIndex - 1].style.display = "block";
        }

        plusSlides(n) {
            this.browsing(this.slideIndex += n);
        }

    }
);
